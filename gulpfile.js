const gulp = require('gulp');
const postcss = require('gulp-postcss');
const px2rem = require('postcss-px2rem');
const cssmin = require('gulp-cssmin');
const rename = require("gulp-rename");

const less = require('gulp-less');
gulp.task('default', function() {
  var processors = [px2rem({remUnit: 75})];
  return gulp.src('./components/styles.css')
    .pipe(less())
    .pipe(postcss(processors))
    .pipe(cssmin())
    .pipe(rename("k-ui-rem.css"))
    .pipe(gulp.dest('./dist'));
});