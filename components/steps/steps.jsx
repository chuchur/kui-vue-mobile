import { getChild } from '../_tool/utils'
// import StepItem from './stepItem'
export default {
  name: 'Steps',
  props: {
    title: String,
    current: { type: Number, default: 0 },
    description: String,
    status: {
      validator: (v) => {
        return ['wait', 'process', 'finish', 'error'].indexOf(v) >= 0
      },
      default: 'wait'
    },
    icon: String
  },
  provide() {
    return {
      Steps: this
    }
  },
  render() {
    let childs = getChild(this.$slots.default)
    childs.map((child, index) => {
      let s;
      if (index == this.current) {
        s = this.status || 'process'
      } else if (index > this.current) {
        s = 'wait'
      } else if (index < this.current) {
        s = 'finish'
      }
      child.componentOptions.propsData.status = s
    })
    return (
      <div class="k-steps">{childs}</div>
    )
  }
}