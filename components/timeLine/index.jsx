export default {
  props: {
    actived: Boolean,
    title: String,
    content: String
  },
  render() {
    return (
      <div class={['k-timeline', { 'k-timeline-actived': this.actived }]}>
        <div div class="k-timeline-head" >
          <span class="k-timeline-icon"></span>
          <span class="k-timeline-title">{this.title}</span>
        </div>
        <div class="k-timeline-content">{this.content}</div>
      </div>
    )
  }
}