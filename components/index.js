import Affix from './affix'
import { Nav, Views } from './nav'
import Button from './button'
import BackTop from './backtop'
import { Checkbox, CheckboxGroup } from './checkbox'
// import Drawer from './drawer'
import Cell from './cell'
import CellAction from './cellaction'
import Modal from './modal'
import Message from './message'
import Popup from './popup'
import TimeLine from './timeLine'
import Loading from './loading'
import Icon from './icon'
import { Radio, RadioGroup, RadioButton } from './radio'
import Switch from './switch'
import { Swiper, SwiperItem } from './swiper'
import { Steps, StepItem } from './steps'
import { Picker, DatePicker } from './picker'

import { version } from '../package.json'

// import './styles';

const Components = {
  Affix,
  BackTop,
  Checkbox, CheckboxGroup,
  Radio, RadioGroup, RadioButton,
  Button,
  Cell, CellAction,
  // Drawer,
  Nav, Views,
  Icon,
  Loading,
  Message,
  Modal,
  Swiper, SwiperItem, Switch,
  Steps, StepItem,
  Popup, Picker, DatePicker,
  TimeLine
}

const UI = {
  ...Components,
  kSwitch: Switch,
  kButton: Button,
  Version: version,
}


const install = function (Vue) {
  for (let key in UI) {
    Vue.component(key, UI[key])
  }
  Vue.prototype.$Modal = Modal;
  Vue.prototype.$Message = Message;
  Vue.prototype.$Loading = Loading;
}
UI.install = install

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}
export default UI
