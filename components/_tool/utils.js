export function isEmptyNode(vnode = {}) {
  return !(vnode.eml || vnode.tag || (vnode.text && vnode.text.trim().length));
}
export function getChild(child = []) {
  return child.filter((c) => !isEmptyNode(c));
}

export function contains(ele, target) {
  //ele是内部元素，target是你想找到的包裹元素
  console.log(ele, "--", target, "111");
  if (!ele || ele === document || !target) return false;
  return ele === target ? true : contains(ele.parentNode, target);
}

export function isNotEmpty(str) {
  return str !== "" && str !== undefined && str !== null;
}

export function hasProp(context, key) {
  const options = context.$options || {};
  const props = options.propsData || {};
  return key in props;
}
export function bigInt(num) {
  return parseFloat((num).toFixed(2))
}
export function getElementPos(element) {
  var parent = element; //.offsetParent;
  let pos = {
    left: 0,
    top: 0,
    width: parent.offsetWidth,
    height: parent.offsetHeight,
  };
  while (parent !== null) {
    pos.left += parent.offsetLeft;
    pos.top += parent.offsetTop;
    parent = parent.offsetParent;
  }
  return pos;
}

export function scrollToOffset(target, offsetY, offsetX) {
  let { scrollTop, scrollLeft } = target;
  if (target == window) {
    scrollTop = window.scrollY
    scrollLeft = window.scrollX
  }
  let x = 0, y = 0;
  let delays = setInterval(() => {
    y = bigInt((offsetY - scrollTop) / 5); //匀减速运动
    if (offsetX != null) {
      x = bigInt((offsetX - scrollLeft) / 5); //匀减速运动
    }
    if (y == 0 && x == 0) {
      clearInterval(delays);
    }
    scrollTop += y;
    scrollLeft += x;
    target.scrollTo(scrollLeft, scrollTop);
    // console.log(offsetX)
  }, 16);
  return delays;
}
