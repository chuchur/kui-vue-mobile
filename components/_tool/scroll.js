import Vue from 'vue';
const SSR = Vue.prototype.$isServer

export default {
  bind(el, { value }) {
    if (SSR) return;
    if (typeof value == 'function') {
      // const target = document.body.offsetHeight == window.innerHeight ? document.body : window
      // console.log(document.body.offsetHeight == window.innerHeight, target)
      window.addEventListener('scroll', value)
    }
  },
  unbind(el, { value }) {
    if (typeof value == 'function') {
      // const target = document.body.offsetHeight == window.innerHeight ? document.body : window
      window.removeEventListener('scroll', value)
    }
  }
}