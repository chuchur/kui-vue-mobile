export function isObject(o) {
  return (typeof o === 'object' || typeof o === 'function') && o !== null
}
export function deepCopy(obj){
  if (!isObject(obj)) return obj
  let data = []
  let isArray = Array.isArray(obj)
  let cloneObj = isArray ? [] : {}

  for (let key in obj) {
    cloneObj[key] = isObject(obj[key]) ? deepCopy(obj[key]) : obj[key]
  }

  return cloneObj
}