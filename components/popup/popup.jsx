import Icon from '../icon'
import transfer from "../_tool/transfer";

export default {
  name: 'Popup',
  directives: { transfer },
  props: {
    value: { type: Boolean, default: false },
    title: String,
    closeable: { type: Boolean, default: true },
  },
  data() {
    return {
      visible: this.value,
    };
  },
  watch: {
    value(v) {
      if (v) {
        this.visible = v
        // window.document.getElementsByTagName('html')[0].style.overflow = 'hidden'
        // window.document.body.style.overflow = 'hidden'
      } else {
        // window.document.getElementsByTagName('html')[0].style.overflow = ''
        // window.document.body.style.overflow = ''
        this.close();
      }
    }
  },
  methods: {
    touchmove(e) {
      e.preventDefault();
      e.stopPropagation();
    },
    close() {
      this.visible = false;
      this.$emit("close");
      this.$emit("input", false);
    }
  },
  render() {
    let { visible, title, close } = this
    return (
      <div class="k-popup" v-transfer={true}>
        <transition name="fade">
          <div class="k-popup-mask" v-show={visible} onClick={close} onTouchMove={this.touchmove}></div>
        </transition>
        <transition name="popup-slide">
          <div class="popup-content" v-show={visible}>
            {this.closeable ? <span class="k-popup-close" onClick={close}><Icon type="ios-close" /></span> : null}
            {title ? <div class="k-popup-title">{title}</div> : null}
            <div class="popup-content-inner">
              {this.$slots.default}
            </div>
          </div>
        </transition>
      </div>
    )
  }
}

