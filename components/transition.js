const on = {
  beforeEnter(el) {
    // el.style.overflow = 'hidden';
    el.style.height = 0
    el.style.opacity = 0.1
  },
  enter(el) {
    if (el.scrollHeight !== 0) {
      el.style.height = el.scrollHeight + 'px'
      el.style.opacity = 1
    } else {
      el.style.height = ''
      el.style.opacity = ''
    }
  },
  afterEnter(el) {
    el.style.height = ''
    el.style.opacity = ''
  },
  beforeLeave(el) {
    el.style.height = el.scrollHeight + 'px'
    el.style.opacity = 1
  },
  leave(el) {
    if (el.scrollHeight !== 0) {
      el.style.height = 0;
      el.style.paddingTop = 0;
      el.style.paddingBottom = 0;
      el.style.marginTop = 0;
      el.style.marginBottom = 0;
      el.style.opacity = 0
    }
  },
  afterLeave(el) {
    el.style.height = '';
    el.style.paddingTop = '';
    el.style.paddingBottom = '';
    el.style.marginTop = '';
    el.style.marginBottom = '';
    el.style.opacity = ''
    el.style.overflow = ''
  },
}

export default {
  name: "Transition",
  functional: true, //为true 表示 无状态 data 无 无实例 没有this
  render(h, context) {
    return h('transition', { on }, context.children)
  }
} 