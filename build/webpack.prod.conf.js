/**
 * by chuchur /chuchur@qq.com
 * 打包vue 组件
 */
const MiniCssExtractPlugin = require('mini-css-extract-plugin') //for webpack 4
const UglifyJsPlugin = require('uglifyjs-webpack-plugin'); //for webpack 4
const TerserPlugin = require('terser-webpack-plugin');

const path = require('path');
const webpackBaseConfig = require('./webpack.base.conf.js');
const { merge } = require('webpack-merge');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const WebpackBar = require('webpackbar')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = merge(webpackBaseConfig, {
	mode: 'production',
	entry: {
		main: path.resolve(__dirname, '../components/index.js')
	},
	output: {
		path: path.resolve(__dirname, "../dist"),
		publicPath: "",
		filename: "k-ui.js",
		library: 'kui-vue',
		libraryTarget: 'umd',
		umdNamedDefine: true
	},
	performance: {
		hints: false
	},
	module: {
		rules: [
			// {
			// 	test: /\.less$/,
			// 	use: [
			// 		MiniCssExtractPlugin.loader,
			// 		// {
			// 		// 	loader: 'px2rem-loader',
			// 		// 	// options here
			// 		// 	options: {
			// 		// 		remUni: 75,
			// 		// 		remPrecision: 8
			// 		// 	}
			// 		// },
			// 		'css-loader',
			// 		'postcss-loader',
			// 		'less-loader',
			// 	], // : , 
			// },
			// {
			// 	test: /\.css$/,
			// 	use: [{
			// 		loader: 'style-loader'
			// 	}, {
			// 		loader: 'css-loader'
			// 	}, {
			// 		loader: 'px2rem-loader',
			// 		// options here
			// 		options: {
			// 			remUni: 75,
			// 			remPrecision: 8
			// 		}
			// 	}]
			// },

		]
	},
	externals: {
		"vue": "Vue",
		'vue-router': 'VueRouter',
		"kui-icons": "kui-icons",
		'moment': 'moment'
	},
	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				uglifyOptions: {
					cache: true,
					parallel: true,
					sourceMap: true,
					uglifyOptions: {
						warnings: false,
					},
				}
			}),
			// new TerserPlugin({
			// 	cache: true,
			// 	parallel: true,
			// 	sourceMap: true,

			// 	extractComments: false,
			// }),
			new OptimizeCSSAssetsPlugin({})
		]
	},
	plugins: [
		new WebpackBar({
			name: '🚙  K UI a vue components',
			color: 'green',
		}),
		new MiniCssExtractPlugin({ filename: 'k-ui.css' }),
		new CleanWebpackPlugin()
	],

})