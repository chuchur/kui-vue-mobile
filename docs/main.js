import Vue from "vue";
import App from "./app";
import router from "./router";

import Components from '../components/index.js'
Vue.use(Components)

import "./assets/demo.less";

new Vue({
  el: "#app",
  router: router,
  render: (h) => h(App),
  // store
});
